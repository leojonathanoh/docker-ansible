# docker-ansible

[![github-actions](https://github.com/leojonathanoh/docker-ansible/workflows/build/badge.svg)](https://github.com/leojonathanoh/docker-ansible/actions)
[![docker-image-size](https://img.shields.io/microbadger/image-size/leojonathanoh/docker-ansible/latest)](https://hub.docker.com/r/leojonathanoh/docker-ansible)
[![docker-image-layers](https://img.shields.io/microbadger/layers/leojonathanoh/docker-ansible/latest)](https://hub.docker.com/r/leojonathanoh/docker-ansible)

Dockerized `ansible` alpine image with some optional tools

| Tags |
|:-------:| 
| `:v2.3.0.0-alpine-3.6` | 
| `:v2.3.0.0-ssh-alpine-3.6` | 
| `:v2.4.6.0-alpine-3.7` | 
| `:v2.4.6.0-ssh-alpine-3.7` | 
| `:v2.6.19-alpine-3.8` | 
| `:v2.6.19-ssh-alpine-3.8` | 
| `:v2.7.13-alpine-3.9` | 
| `:v2.7.13-ssh-alpine-3.9` | 
| `:v2.8.4-alpine-3.10` | 
| `:v2.8.4-ssh-alpine-3.10` |
